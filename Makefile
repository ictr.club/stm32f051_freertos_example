
COMPILER = GCC
ARCH = ARM_CM0
MCU = stm32f051
DEBUG ?= 1
PROJECT ?= stm32f0_frtos

OUTPUT_DIR = ./output
PERIPH_LIB = ./stm32f0_spl
SPL_LIB_PATH = $(PERIPH_LIB)/STM32F0xx_StdPeriph_Driver
RTOS_PATH = ./FreeRTOS-Kernel
RTOS_PORTABLE_PATH = $(RTOS_PATH)/portable

IPATH += ./inc
IPATH += $(PERIPH_LIB)/CMSIS/Include
IPATH += $(PERIPH_LIB)/CMSIS/Device/Include
IPATH += $(SPL_LIB_PATH)/inc
IPATH += $(RTOS_PATH)/include
IPATH += $(RTOS_PORTABLE_PATH)/$(COMPILER)/$(ARCH)

VPATH += ./src
VPATH += $(RTOS_PATH)
VPATH += $(SPL_LIB_PATH)/src
VPATH += $(RTOS_PORTABLE_PATH)
VPATH += $(RTOS_PORTABLE_PATH)/$(COMPILER)/$(ARCH)
VPATH += $(PERIPH_LIB)/CMSIS/Device/Source/gcc

#Assembly init dosyasi dahil ediliyor
SRCS += startup_$(MCU).s

#FreeRTOS kerneli dahil ediliyor
SRCS += tasks.c queue.c list.c
SRCS += $(RTOS_PORTABLE_PATH)/$(COMPILER)/$(ARCH)/port.c

#Standard Peripheral Library dahil ediliyor
SRCS += $(wildcard $(SPL_LIB_PATH)/src/*.c)

SRCS += main.c
SRCS += system_stm32f0xx.c
#Derleyiciye kodlarimiza gecmesi icin verecegimiz flaglar
PROJ_CFLAGS += USE_STDPERIPH_DRIVER=1

#Compiler Options
CC = arm-none-eabi-gcc
CPP = arm-none-eabi-g++
AR = arm-none-eabi-ar
LD = arm-none-eabi-gcc
OBJDUMP=arm-none-eabi-objdump
OBJCOPY=arm-none-eabi-objcopy

CFLAGS += \
	-mthumb					\
	-mcpu=cortex-m0			\
	-DSTM32F051=1			\
       -Wa,-mimplicit-it=thumb			\
       -O0			\
       -ffunction-sections			\
       -fdata-sections				\
		-MD 					\
       -Wall					\
       -Wno-format				\
	   -DUSE_STDPERIPH_DRIVER=1	\
       -c

ifeq ($(DEBUG), 1)
CFLAGS+=-g3 -ggdb -DDEBUG
endif

CFLAGS += $(foreach incpath, $(PROJ_CFLAGS), -D$(incpath))
CFLAGS += $(foreach incpath, $(IPATH), -I$(incpath))

#Linker Options
ENTRY=Reset_Handler
STD_LIBS=-lc -lm
LINKERFILE=$(MCU).ld
LDFLAGS=-mthumb                                                                \
        -mcpu=cortex-m0                                                        \
        -Xlinker --gc-sections --specs=nano.specs                                     \
	-Xlinker -Map -Xlinker ${OUTPUT_DIR}/output.map


#Generate parameters from SRCS
SRCS_NO_PATH = $(notdir $(SRCS))

SRCS_O := $(SRCS_NO_PATH:%.c=%.o)
SRCS_O := $(SRCS_O:%.S=%.o)
SRCS_O := $(SRCS_O:%.s=%.o)
SRCS_O := $(SRCS_O:%.o=$(OUTPUT_DIR)/%.o)

SRCS_D := $(SRCS_O:%.o=%.d)

.PHONY: test
test:
	@printf "Include Paths: \n $(foreach incpath,$(IPATH),$(incpath)\n) \n\n"
	@printf "Source Paths: \n $(foreach srcpath,$(VPATH),$(srcpath)\n) \n\n"
	@printf "Source Files: \n $(foreach srcpath,$(SRCS_NO_PATH),$(srcpath)\n) \n \n"
	@printf "Depedency Files: \n $(foreach srcpath,$(SRCS_D),$(srcpath)\n) \n \n"
	@printf "Compiler Options: \n $(CFLAGS) \n\n"


create_output_dir:
	@mkdir -p ${OUTPUT_DIR}

${OUTPUT_DIR}/%.d: %.c
	@printf "${CC} ${CFLAGS} -E  ${@} ${<} \n"
	@${CC} ${CFLAGS} -E -o ${@} ${<}

${OUTPUT_DIR}/%.o: %.c
	@printf "${CC} ${CFLAGS} -o ${@} ${<} \n\n"
	@${CC} ${CFLAGS} -o ${@} ${<}

${OUTPUT_DIR}/%.d: %.s
	@printf "${CC} ${CFLAGS} -E  ${@} ${<} \n"
	@${CC} ${CFLAGS} -E -o ${@} ${<}

${OUTPUT_DIR}/%.o: %.s
	@printf "${CC} ${CFLAGS} -o ${@} ${<} \n\n"
	@${CC} ${CFLAGS} -o ${@} ${<}

${OUTPUT_DIR}/%.d: %.S
	@printf "${CC} ${CFLAGS} -E  ${@} ${<} \n"
	@${CC} ${CFLAGS} -E -o ${@} ${<}

${OUTPUT_DIR}/%.o: %.S
	@printf "${CC} ${CFLAGS} -o ${@} ${<} \n\n"
	@${CC} ${CFLAGS} -o ${@} ${<}

hex: ${OUTPUT_DIR}/$(PROJECT).elf
	@printf "$(OBJCOPY) -O ihex $< > $(BASE_OUTPUT_DIR)/$(PROJECT).hex \n\n"
	$(OBJCOPY) -O ihex $< ${OUTPUT_DIR}/$(PROJECT).hex

size: ${OUTPUT_DIR}/$(PROJECT).elf
	@echo 'Invoking: GNU ARM Cross Print Size'
	arm-none-eabi-size --format=berkeley "${OUTPUT_DIR}/$(PROJECT).elf"
	@echo 'Finished building: $@'
	@echo ' '

${OUTPUT_DIR}/%.elf: ${SRCS_O} ${LINKERFILE}
	@printf "\n Linker has started \n\n"
	@printf "\n ${LD} -T ${LINKERFILE} --entry ${ENTRY} ${LDFLAGS} -o ${@} ${SRCS_O} -Xlinker --start-group $(filter %.a, ${^}) ${STD_LIBS} -Xlinker --end-group \n\n"
	@${LD} -T ${LINKERFILE} --entry ${ENTRY} ${LDFLAGS} -o ${@} ${SRCS_O} -Xlinker --start-group $(filter %.a, ${^}) ${STD_LIBS} -Xlinker --end-group
	@printf "\n Linker finished the job \n\n"


${OUTPUT_DIR}/$(PROJECT).elf: ${SRCS_O} ${LINKERFILE}

.PHONY: all
all: create_output_dir ${SRCS_D} ${OUTPUT_DIR}/$(PROJECT).elf
all: hex size

.PHONY: clean
clean: 
	@rm -rf ./output/*